/*------------------------------------------------------------------------------
 * MDK Middleware - Component ::USB:Device
 * Copyright (c) 2004-2015 ARM Germany GmbH. All rights reserved.
 *------------------------------------------------------------------------------
 * Name:    Audio.c
 * Purpose: USB Device Audio Example
 *----------------------------------------------------------------------------*/

#include "cmsis_os.h"                   /* CMSIS RTOS definitions             */

#include "Board_GLCD.h"
#include "Board_Audio.h"
#include "GLCD_Config.h"

#include "stm32f7xx_hal.h"

#include <math.h>
#include "arm_math.h"


extern GLCD_FONT GLCD_Font_6x8;
extern GLCD_FONT GLCD_Font_16x24;

extern uint32_t os_time;

#define  PLAYBACK_CHANNELS                     2        // Stereo speakers = 2 channels (mono not implemented)
#define  PLAYBACK_BUFFER_SIZE_AUDIO          1024*32      // Out samples (128 stereo samples at 32 kHz = 2 ms samples)

#define N 1000
float32_t x_n[N]; 

static volatile bool       play                                      = false;
static          uint16_t   play_buf_index                           =   0  ;
static          int16_t    play_buf[PLAYBACK_BUFFER_SIZE_AUDIO] = { 0 };

uint32_t HAL_GetTick (void) {
  return os_time;
}

/**
  * System Clock Configuration
  *   System Clock source            = PLL (HSE)
  *   SYSCLK(Hz)                     = 216000000
  *   HCLK(Hz)                       = 216000000
  *   AHB Prescaler                  = 1
  *   APB1 Prescaler                 = 4
  *   APB2 Prescaler                 = 2
  *   HSE Frequency(Hz)              = 25000000
  *   PLL_M                          = 25
  *   PLL_N                          = 432
  *   PLL_P                          = 2
  *   PLL_Q                          = 9
  *   VDD(V)                         = 3.3
  *   Main regulator output voltage  = Scale1 mode
  *   Flash Latency(WS)              = 7
  */
static void SystemClock_Config (void) {
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_PeriphCLKInitTypeDef RCC_ExCLKInitStruct;

  /* Enable HSE Oscillator and activate PLL with HSE as source */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSIState = RCC_HSI_OFF;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 25;
  RCC_OscInitStruct.PLL.PLLN = 432;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 9;
  HAL_RCC_OscConfig(&RCC_OscInitStruct);

  /* Activate the OverDrive to reach the 216 MHz Frequency */
  HAL_PWREx_EnableOverDrive();

  /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2 clocks dividers */
  RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;
  HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_7);

  HAL_RCCEx_GetPeriphCLKConfig(&RCC_ExCLKInitStruct);
  RCC_ExCLKInitStruct.PeriphClockSelection = RCC_PERIPHCLK_SAI2;
  RCC_ExCLKInitStruct.Sai2ClockSelection = RCC_SAI2CLKSOURCE_PLLI2S;
  RCC_ExCLKInitStruct.PLLI2S.PLLI2SP = 8;
  RCC_ExCLKInitStruct.PLLI2S.PLLI2SN = 344;
  RCC_ExCLKInitStruct.PLLI2S.PLLI2SQ = 7;
  RCC_ExCLKInitStruct.PLLI2SDivQ = 1;
  HAL_RCCEx_PeriphCLKConfig(&RCC_ExCLKInitStruct);
}

/**
  * Configure the MPU attributes as Write Through for SRAM1/2
  *   The Base Address is 0x20010000 since this memory interface is the AXI.
  *   The Region Size is 256KB, it is related to SRAM1 and SRAM2 memory size.
  */
static void MPU_Config (void) {
  MPU_Region_InitTypeDef MPU_InitStruct;

  /* Disable the MPU */
  HAL_MPU_Disable();

  /* Configure the MPU attributes as WT for SRAM */
  MPU_InitStruct.Enable = MPU_REGION_ENABLE;
  MPU_InitStruct.BaseAddress = 0x20010000;
  MPU_InitStruct.Size = MPU_REGION_SIZE_256KB;
  MPU_InitStruct.AccessPermission = MPU_REGION_FULL_ACCESS;
  MPU_InitStruct.IsBufferable = MPU_ACCESS_NOT_BUFFERABLE;
  MPU_InitStruct.IsCacheable = MPU_ACCESS_CACHEABLE;
  MPU_InitStruct.IsShareable = MPU_ACCESS_NOT_SHAREABLE;
  MPU_InitStruct.Number = MPU_REGION_NUMBER0;
  MPU_InitStruct.TypeExtField = MPU_TEX_LEVEL0;
  MPU_InitStruct.SubRegionDisable = 0x00;
  MPU_InitStruct.DisableExec = MPU_INSTRUCTION_ACCESS_ENABLE;

  HAL_MPU_ConfigRegion(&MPU_InitStruct);

  /* Enable the MPU */
  HAL_MPU_Enable(MPU_PRIVILEGED_DEFAULT);
}

/**
  * CPU L1-Cache enable
  */
static void CPU_CACHE_Enable (void) {

  /* Enable I-Cache */
  SCB_EnableICache();

  /* Enable D-Cache */
  SCB_EnableDCache();
}

// Audio Events Handling Callback function
// \param[in]   event  notification mask
static void AudioCallback (uint32_t event) {
 
  // Handling of playback samples
  if (event & AUDIO_EVENT_SEND_COMPLETE) {
        // Start new audio data transmission
		 Audio_SendData (&play_buf[play_buf_index], 128);
		 play_buf_index+=128;
		 if(play_buf_index>=PLAYBACK_BUFFER_SIZE_AUDIO)
			 //Audio_Stop(AUDIO_STREAM_OUT);
		 play_buf_index=0;
	}  
}

static int16_t value;

void Initialize (void) {
  // Initialize audio codec for: 
  //   - stereo speakers
  //   - sampling rate 32 kHz
  //   - 16 bits per sample
  //   - not muted
  //   - volume at 50 %
  //
  //  NOTE !!!
  //  - These values must correspond to settings in USBD_Config_ADC_0.h file
  Audio_Initialize        (&AudioCallback);
  Audio_SetDataFormat     (AUDIO_STREAM_OUT, AUDIO_DATA_16_STEREO);
  Audio_SetFrequency      (AUDIO_STREAM_OUT, 32000);
  Audio_SetMute           (AUDIO_STREAM_OUT, AUDIO_CHANNEL_MASTER, false);
  Audio_SetVolume         (AUDIO_STREAM_OUT, AUDIO_CHANNEL_MASTER, 50);
  Audio_Start             (AUDIO_STREAM_OUT);

}

/*------------------------------------------------------------------------------
 *        Application
 *----------------------------------------------------------------------------*/
int main (void) {
	int i=0;

  MPU_Config();                         /* Configure the MPU                  */
  CPU_CACHE_Enable();                   /* Enable the CPU Cache               */
  HAL_Init();                           /* Initialize the HAL Library         */
  SystemClock_Config();                 /* Configure the System Clock         */

  GLCD_Initialize         ();
  GLCD_SetBackgroundColor (GLCD_COLOR_BLUE);
  GLCD_SetForegroundColor (GLCD_COLOR_WHITE);
  GLCD_ClearScreen        ();
  GLCD_SetFont            (&GLCD_Font_16x24);
  GLCD_DrawString         (0, 0*24, "    Audio Playback  ");
  GLCD_DrawString         (0, 1*24, "                    ");
  GLCD_DrawString         (0, 2*24, "                    ");
  GLCD_DrawString         (0, 5*24, "                    ");
  GLCD_DrawString         (0, 8*24, "  Keil Tools by ARM ");
  GLCD_DrawString         (0, 9*24, "    www.keil.com    ");

	//TODO calculate 600Hz and place in play_buf
	
	
	Initialize();
	
	//sending first 128 samples
	Audio_SendData (&play_buf[play_buf_index], 128);
	play_buf_index+=128;


  while (1) {
    osDelay(10000);
  }
}
